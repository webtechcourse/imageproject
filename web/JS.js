/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    getImgs();

    $('#upload').click(function () {
        $('#myMod').modal('show');

    });


    $("#search").click(function () {
        console.log("get userImages");
        userID = $("#searchbar").val();
        getUserImages(userID);
        event.stopPropagation();
    });

    /*
     * Gallery Toolbar functionality by Julius begins here
     */

    function isTouchDevice() {
        try {
            document.createEvent("TouchEvent");
            return true;
        } catch (e) {
            return false;
        }
    }

    function hideOthers(caller) {
        var callerImage = caller.closest(".image");
        $(".hideable").each(function () {
            var image = $(this).closest(".image");
            if (!$(image).is(callerImage)) {
                $(this).hide();
            }
        });
    }

    function blurOthers(caller) {
        $(".image").each(function () {
            if (!$(this).is(caller) && $(this).hasClass("focused")) {
                $(this).removeClass("focused");
                console.log("blurred " + $(this).attr("id"));
            }
        });
    }

    function hideOthersExceptFocused(caller) {
        var callerImage = caller.closest(".image");
        $(".hideable").each(function () {
            var image = $(this).closest(".image");
            if (!($(image).is(callerImage) || $(image).hasClass("focused"))) {
                $(this).hide();
            }
        });
    }


    $(".hotArea").click(function (e) {
        var image = $(this).closest(".image");
        $(image).addClass("focused");
        console.log("focused " + $(this).closest(".image").attr("id"));
        blurOthers(this);
        e.stopPropagation();
        hideOthersExceptFocused(this);
    });

    $(".hotArea").mouseenter(function (e) {
        e.stopPropagation();
        var id = $(this).closest(".image").attr("id"),
                imagePanel = $(this).find(".imagePanel");
        console.log("entered hotarea " + id);
        hideOthersExceptFocused(this);
        $(imagePanel).show();
    });

    $(".hotArea").mouseleave(function (e) {
        e.stopPropagation();
        var id = $(this).closest(".image").attr("id"),
                imagePanel = $(this).find(".imagePanel"),
                $actionPanel = $(imagePanel).find(".actionPanel");
        console.log("left hotarea " + id);
        if ($actionPanel.is(":hidden")) {
            $(imagePanel).hide();
        }
    });

    $(".postcommentButton").click(function () {
        var id = $(this).closest(".image").attr("id"),
                imagePanel = $(this).closest(".imagePanel"),
                $actionPanel = $(imagePanel).find(".actionPanel");

        hideOthers(this);
        $(imagePanel).find($(".postcommentPanel").show(function () {
            if ($actionPanel.is(":hidden")) {
                $actionPanel.show("fast");
            }
        }));
    });

    $(".readcommentsButton").click(function () {
        var id = $(this).closest(".image").attr("id"),
                imagePanel = $(this).closest(".imagePanel"),
                $actionPanel = $(imagePanel).find(".actionPanel");
        hideOthers(this);

        $(imagePanel).find($(".readcommentsPanel")).show(function () {
            if ($actionPanel.is(":hidden")) {
                $actionPanel.show("fast");
            }
        });
    });

    $(".favoriteButton").click(function () {
        var image = (this).closest(".image");
        if ($(image).hasClass("favorited")) {
            $(image).removeClass("favorited");
            $(this).attr("value", "Favorite");
        } else {
            $(image).addClass("favorited");
            $(this).attr("value", "Favorited!");
        }
        $(image).addClass("favorited");
        hideOthers(this);
    });

    $("html").click(function () {
        $(".hideable").hide();
    });

/* 
 * End of Gallery Toolbar functionality 
 * */

});

function getImgs() {
    $.ajax({
        type: "GET",
        url: "http://127.0.0.1:8080/ImgShareApp/main/gallery/view/",
        dataType: "text",
        success: function (response) {
            console.log(response);
            //check the output in the console.
            var s = response.replace('[', '').replace(']', '').replace(/\,/g, '');
            //removes the "[", "]", and "," characters from the response Array.
            $('#imgs').html(s);
            //set the html of the div with id 'imgs' as the response variable. 
            $('#imgs img').each(function () {
                //for each img in the div with id 'Imgs'
                $(this).attr('height', '230px');
                $(this).attr('width', '230px');
                //Setting custom dimensions for the images.
            });
        }
    });
}

function getUserImages(user) {
    console.log("Finding user's images " + user);
    $.getJSON("http://127.0.0.1:8080/ImgShareApp/main/gallery/findUserImages/" + user, function (data) {
        console.log("getJSON success");
        $.each(data, function (index, value) {
            console.log("index " + index + " value " + value);
            $.each(value, function (index, value) {
                console.log("Nr2 index " + index + " value " + value);
                if (index === "TITLE") {
                    console.log("this is the compare for image " + index.localeCompare("TITLE"));
                    console.log("image found " + index);
                    $("#imgs").append("<p>" + value + "</p><br>");
                } else if (index === "PIC") {
                    console.log("Title found " + index);
                    $("#imgs").append("<img src='http://127.0.0.1:8888/uploads/" + value + "' width='250px' height='250px'><br>");
                } else if (index === "ID") {
                    console.log("ID found " + index);
                }
            });
        });
    });

}


function getGallery() {
    $.ajax({
        type: "GET",
        url: "http://127.0.0.1:8080/ImgShareApp/main/gallery/view/",
        dataType: "text",
        success: function (response) {
            console.log(response);
            //check the output in the console.
            var s = response.replace('[', '').replace(']', '').replace(/\,/g, '');
            //removes the "[", "]", and "," characters from the response Array.
            $('#imgs').html(s);
            //set the html of the div with id 'imgs' as the response variable. 
            $('#imgs img').each(function () {
                //for each img in the div with id 'Imgs'
                $(this).attr('height', '230px');
                $(this).attr('width', '230px');
                //Setting custom dimensions for the images.
            });
        }
    });
}    