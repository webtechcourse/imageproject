/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imgpackage;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author iosdev
 */
@Entity
@Table(name = "PASSWORD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Password.findAll", query = "SELECT p FROM Password p"),
    @NamedQuery(name = "Password.findByPass", query = "SELECT p FROM Password p WHERE p.pass = :pass"),
    @NamedQuery(name = "Password.findByUid", query = "SELECT p FROM Password p WHERE p.uid = :uid"),
    @NamedQuery(name = "Password.findByPassid", query = "SELECT p FROM Password p WHERE p.passid = :passid")})
public class Password implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 25)
    @Column(name = "PASS")
    private String pass;
    @Column(name = "UID")
    private Integer uid;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PASSID")
    private Integer passid;

    public Password() {
    }

    public Password(Integer passid) {
        this.passid = passid;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getPassid() {
        return passid;
    }

    public void setPassid(Integer passid) {
        this.passid = passid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (passid != null ? passid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Password)) {
            return false;
        }
        Password other = (Password) object;
        if ((this.passid == null && other.passid != null) || (this.passid != null && !this.passid.equals(other.passid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "imgpackage.Password[ passid=" + passid + " ]";
    }
    
}
