/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imgpackage;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author iosdev
 */
@Path("gallery")
public class GalleryResource {
    
    EntityManagerFactory emf;
    EntityManager em;
    private int searchUID;
    private int searchIID;
    private int searchTID;
    private int randomIID;
    private User newUser;
    private User queryUser;
    private Picture foundImage;
    private Picture queryImage;
    private Comment newComment;
    
    JsonArray jsonImageArray;
    JsonArray emptyJsonArray;
    JsonObject jsonImageObject;
    JsonObjectBuilder jsonObjectBuilder;
    
    private ArrayList<Picture> userImageArray;
    private ArrayList<String> userArrayList;
    private ArrayList<String> galleryImages;
    private ArrayList<Integer> imageRatings;
    private Collection<User> userCollection;
    private Collection<Picture> imageCollection;
    private Collection<Comment> commentCollection;
    private List<Picture> userImageList;
    private List<Picture> imageList;
    private List<Picture> imageQuery;
    private List<Picture> imageQueryJSON;
    private List<User> userList;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GalleryResource
     */
    public GalleryResource() {
    }

    /**
     * Retrieves representation of an instance of imgpackage.GalleryResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("view")
    @Produces("text/html")
    public String viewGallery() {
        
        createTransaction();
        //create transaction method (see bottom of class)
        
        List<Picture> images = em.createNamedQuery("Picture.findAll").getResultList();
        galleryImages = new ArrayList();
        
        for (Picture i : images) {
            if (i.getPic().endsWith("jpg") || i.getPic().endsWith("png") || i.getPic().endsWith("jpeg")) {
                //checks the file extension to make sure that it shows only images.
                // old: galleryImages.add("<img src=\"http://127.0.0.1:8888/uploads/" + i.getPic() + "\" >");
                galleryImages.add("" + 
                        "<div class=\"image hotArea\" id=\"" + i.getPic() + "\">\n" +
"    <!-- single image wrapper -->\n" +
"    <img src=\"http://127.0.0.1:8888/uploads/" + i.getPic() + "\" />\n" +
"    <div class=\"imagePanel hideable\">\n" +
"        <!-- wraps all toolbar elements and action panels for image -->\n" +
"\n" +
"        <div class=\"imageToolbar\">\n" +
"            <!-- simple image toolbar, shows on hover/tap -->\n" +
"\n" +
"            <form class=\"toolbarElement\">\n" +
"                <input class=\"postcommentButton\" type=\"button\" value=\"Post Comment\" />\n" +
"            </form>\n" +
"            <form class=\"toolbarElement\">\n" +
"                <input class=\"readcommentsButton\" type=\"button\" value=\"Read Comments\" />\n" +
"            </form>\n" +
"            <form class=\"toolbarElement\">\n" +
"                <input class=\"favoriteButton\" type=\"button\" value=\"Favorite\" />\n" +
"            </form>\n" +
"        </div>\n" +
"\n" +
"        <div class=\"actionPanel hideable\">\n" +
"            <!-- wraps all action panels -->\n" +
"            <div class=\"postcommentPanel hideable\">\n" +
"                <form name=\"commentForm\" id=\"" + i + "\" method=\"POST\">\n" +
"                    <input type=\"hidden\" name=\"imgID\" id=\"imgID\" value=\"" + i.getPic() + "\">\n" +
"                    <input type=\"text\" class=\"textInput\" name=\"name\" placeholder=\"Username\" required=\"required\">\n" +
"                    <textarea name=\"comment\" class=\"textInput\" placeholder=\"Comment\" required=\"required\"></textarea>\n" +
"                    <input type=\"submit\" name=\"submit\" value=\"Post Comment\">\n" +
"                </form>\n" +
"            </div>\n" +
"            <div class=\"readcommentsPanel hideable\">\n" +
"                <!-- posted comments are displayed here -->\n" +
"            </div>\n" +
"        </div>\n" +
"    </div>\n" +
"</div>");
                
                //adds a line of HTML to an array of images, which are then displayed.
            } else {
                galleryImages.add("Found upload with title " + i.getTitle() + " that is not an image <br>.");
                
            }
        }
         
        endTransaction();
        //end transaction method (see bottom of class)
        return galleryImages.toString();
        //returns the array of images as a string, this allows for the JavaScript code to use it as HTML.
        
    }
    
    
     //Not working found using a Servlet to be more effective.  
/*    @POST
    @Path("registerUser")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String registerUser(@FormParam("username") String username,
            @FormParam("password") String password) {
        
        createTransaction();
        this.userList = em.createNamedQuery("User.findAll").getResultList();
        for (User u : this.userList) {
            if (u.getUsername().equals(username)) {
                endTransaction();
                return "Username already found, add another username";
            }
        }
        newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(password);
        em.persist(newUser);
        // create query for finding the user just created
        for (User p : (List<User>) em.createQuery("SELECT c FROM User c WHERE c.username LIKE :USERNAME").setParameter("USERNAME", username).getResultList()) {
            return "Found just created user from database " + p.getUsername() + " UID " + p.getId();
        }
        endTransaction();
        //if the user creation failed
        return "Somethign went wrong! User " + newUser.getUsername() + " was not created"; 
    }    
 */ 
    
    //find a specific user's posted images
    @POST
    @Path("findUserImages/{user}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    public JsonArray findUserImages(@FormParam("USERNAME") String Id) {
        this.userImageArray = new ArrayList();
        this.searchUID = Integer.parseInt(Id);
        
        createTransaction();
        
        this.userList = em.createNamedQuery("User.findByUsername").setParameter("USERNAME", this.searchUID).getResultList();
        this.imageQuery = em.createNamedQuery("Image.findAll").getResultList();
        // set the user as null, if there is no user, it can be checked
        // create an empty jsonArray to return, in case the user is not found.
        this.queryUser = null;
        this.emptyJsonArray = Json.createArrayBuilder().add("User not found").build();
        for (User u : this.userList) {
            if (u.getId() == this.searchUID) {
                this.queryUser = u;
            }
        }
        // if the queryUser is still set as null then the user was not found from db
        if (this.queryUser == null) {
            endTransaction();
            return emptyJsonArray;
        }
        for (Picture img : imageQuery) {
            if (img.getUid() == this.queryUser) {
                this.userImageArray.add(img);
            }
        }
        // create a objectBuilder
        JsonObjectBuilder jsonImageObjectBuilder = Json.createObjectBuilder();
        // create a JsonObject
        jsonImageObject = jsonImageObjectBuilder.build();
        // create a JsonArrayBuilder
        JsonArrayBuilder jsonImageArrayBuilder = Json.createArrayBuilder();
        // loop through all the images in users image collection and add them into the JsonObject
        for (Picture img : this.userImageArray) {
            jsonObjectBuilder = jsonImageObjectBuilder.add("PIC", img.getPic());
            jsonObjectBuilder = jsonImageObjectBuilder.add("TITLE", img.getTitle());
            jsonObjectBuilder = jsonImageObjectBuilder.add("POSTUSER", img.getUid().toString());
            // build the JsonObject to finalize it
            jsonImageObject = jsonObjectBuilder.build();
            // add the JsonObject to the ArrayBuilder (same as adding it to the array)
            jsonImageArrayBuilder.add(jsonImageObject);
        }
        // create a JsonArray from the JsonArrayBuilder
        jsonImageArray = Json.createArrayBuilder().add(jsonImageArrayBuilder).add("Array " + this.userImageArray.size() + " size. " + " query user " + this.queryUser.getId()).build();
        endTransaction();
        return jsonImageArray;
    }
    
    
        // gets username and password and checks if matching user is found
    @GET
    @Path("checkUserLogin/{user}/{password}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String checkUserLogin(@PathParam("user") String username,
            @PathParam("password") String password) {
        createTransaction();
        this.userList = em.createNamedQuery("User.findAll").getResultList();
        for (User u : this.userList) {
            if (u.getUsername().equals(username) && u.getPassword().equals(password)) {
                return "" + u.getId();
            }
        }
        return "false";
    }
    
    
    // add comments to a picture
    @GET
    @Path("commentImage/{comment}/{image}/{user}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String commentImage(@PathParam("comment") String comment,
            @PathParam("image") String iid,
            @PathParam("user") String uid) {
        createTransaction();
        this.searchIID = Integer.parseInt(iid);
        this.searchUID = Integer.parseInt(uid);
        // find all users and images
        this.userList = em.createNamedQuery("User.findAll").getResultList();
        this.imageQuery = em.createNamedQuery("Image.findAll").getResultList();
        // finding the user in question
        for (User u : this.userList) {
            if (u.getId() == this.searchUID) {
                this.queryUser = u;
            }
        }
        // find the image in question
        for (Picture i : this.imageQuery) {
            if (i.getId() == this.searchIID) {
                this.queryImage = i;
            }
        }
        // create the new comment
        newComment = new Comment(new Date(), comment, this.queryImage, this.queryUser);
        em.persist(newComment);
        endTransaction();
        return "Comment added";
    }
    
        //get image comments
    @GET
    @Path("getImageComments/{image}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    public JsonArray getImageComments(@PathParam("picture") String ID) {
        this.searchIID = Integer.parseInt(ID);
        createTransaction();
        this.imageQuery = em.createNamedQuery("Picture.findAll").getResultList();
        // setting image as null so if there is no image, it can be checked
        // creating an empty jsonArray to return in case image is not found
        this.queryImage = null;
        this.emptyJsonArray = Json.createArrayBuilder().add("Image couldnt be found").build();
        for (Picture i : this.imageQuery) {
            if (i.getId() == this.searchIID) {
                this.queryImage = i;
            }
        }
        // if the queryImage is still set as null = the image was not found from db
        if (this.queryImage == null) {
            endTransaction();
            return emptyJsonArray;
        }
        // create a objectBuilder
        JsonObjectBuilder jsonImageObjectBuilder = Json.createObjectBuilder();
        // create a JsonObject
        jsonImageObject = jsonImageObjectBuilder.build();
        // create a JsonArrayBuilder
        JsonArrayBuilder jsonImageArrayBuilder = Json.createArrayBuilder();
        // loop through all the comments in the img and add them into the JsonObject
        if (this.queryImage.getCommentCollection().size() >= 1) {
            for (Comment c : this.queryImage.getCommentCollection()) {
                jsonObjectBuilder = jsonImageObjectBuilder.add("COMMENT", c.getText());
                jsonObjectBuilder = jsonImageObjectBuilder.add("USER", c.getUser().getUsername());
                jsonObjectBuilder = jsonImageObjectBuilder.add("TIME", c.getCommenttime().toString());
                // build the JsonObject to finalize it
                jsonImageObject = jsonObjectBuilder.build();
                // add the JsonObject to the ArrayBuilder (same as adding it to the array)
                jsonImageArrayBuilder.add(jsonImageObject);
            }
        } else {
            this.emptyJsonArray = Json.createArrayBuilder().add("No comments").build();
            endTransaction();
            return this.emptyJsonArray;
        }
        // create a JsonArray from the JsonArrayBuilder
        jsonImageArray = Json.createArrayBuilder().add(jsonImageArrayBuilder).build();
        endTransaction();
        return jsonImageArray;
    }
    

    /**
     * PUT method for updating or creating an instance of GalleryResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/html")
    public void putString(String content) {
    }
    
    
    
    public void createTransaction() {
        emf = Persistence.createEntityManagerFactory("ImgShareAppPU");
        em = emf.createEntityManager();
        //em.getTransaction().begin();
    }

    public void endTransaction() {
        //em.getTransaction().commit();
        emf.close();
    }
    
}
