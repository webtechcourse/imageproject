/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imgpackage;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
/**
 *
 * @author iosdev
 */
@WebServlet(name = "ImgServlet", urlPatterns = {"/ImgServlet"})
@MultipartConfig(maxFileSize = 16177215, location = "/var/www/html/uploads")    // upload file's size up to 16MB
public class ImgServlet extends HttpServlet {
     
    // database connection settings
    // private static final int BUFFER_SIZE = 4096;
    private String dbURL = "jdbc:mysql://localhost:3306/ImageSharing?zeroDateTimeBehavior=convertToNull";
    private String dbUser = "potato";
    private String dbPass = "potato";
    
    private Connection getConnection() {
        Connection conn = null;
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            conn = DriverManager.getConnection(dbURL, dbUser, dbPass);
        } catch (Exception e) {
            //wrapping any exception and rethrowing it
            //inside a RuntimeException
            //so the method is silent to exceptions
            throw new RuntimeException("Failed to obtain database connection.", e);
        }
        return conn;
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        
        
        InputStream inputStream = null; // input stream of the upload file
        // obtains the upload file part in this multipart request
        
        Part filePart = request.getPart("photo");
        PrintWriter out = response.getWriter();
        
        
        request.getPart("photo").write(getFilename(request.getPart("photo")));
       
        
        
        String filename = (getFilename(request.getPart("photo")));
        String imgTitle = request.getParameter("imgTitle");
        
        
        Connection conn = null; // connection to the database
        String message = null; // message will be sent back to client
        try {
            // connects to the database
            conn = getConnection();
            // constructs SQL statement
            
            String sql2 = "INSERT INTO PICTURE (PIC, TITLE) values (?, ?)";
            PreparedStatement pic = conn.prepareStatement(sql2);
           
            pic.setString(1, filename);
            pic.setString(2, imgTitle);
                
            //sends the statement to the database server
            int row2 = pic.executeUpdate();
            
            if (row2 > 0 ) {
            out.println("<h3>File uploaded successfully!</h3>");
            out.println("<input type=\"button\" onclick=\"location.href='http://127.0.0.1:8080/ImgShareApp/index.html';\" value=\"Continue\" />");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            
        } finally {
            if (conn != null) {
                // closes the database connection
                try {
                    conn.close();
                } catch (SQLException ex) {
                    //silent
                }
            }
            // sets the message in request scope
            /* request.setAttribute("message", message);

            // forwards to the message page
            getServletContext().getRequestDispatcher("/Message.jsp")
                .include(request, response);
                    
                   */
        }
    }

    private String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }

}