/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imgpackage;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author iosdev
 */
@WebServlet(name = "UserServlet", urlPatterns = {"/UserServlet"})
@MultipartConfig(maxFileSize = 16177215, location = "/var/www/html/uploads")
public class UserServlet extends HttpServlet {
    
    

    private String dbURL = "jdbc:mysql://localhost:3306/ImageSharing?zeroDateTimeBehavior=convertToNull";
    private String dbUser = "potato";
    private String dbPass = "potato";
    
    
    private Connection getConnection() {
        Connection conn = null;
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            conn = DriverManager.getConnection(dbURL, dbUser, dbPass);
        } catch (Exception e) {
            //wrapping any exception and rethrowing it
            //inside a RuntimeException
            //so the method is silent to exceptions
            throw new RuntimeException("Failed to obtain database connection.", e);
        }
        return conn;
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        //get values of text fields
        String username = request.getParameter("username");
        String name = request.getParameter("password");
        
        PrintWriter out = response.getWriter();
        
        Connection conn = null; // connection to the database
        String message = null; // message will be sent back to client
        try {
            // connects to the database
            conn = getConnection();
            // constructs SQL statement
            String sql = "INSERT INTO USER (USERNAME, PASSWORD) values (?, ?)";
            //Using a PreparedStatement to save the file
            PreparedStatement pstmtSave = conn.prepareStatement(sql);
            pstmtSave.setString(1, username);
            pstmtSave.setString(2, name);
            
                
            //sends the statement to the database server
            int row = pstmtSave.executeUpdate();
            
            
            if (row > 0 ) {
                out.println("<h3>File uploaded successfully!</h3>");
                out.println("<input type=\"button\" onclick=\"location.href='http://127.0.0.1:8080/ImgShareApp/index.html';\" value=\"Continue\" />");
            }

        } catch (SQLException ex) {
             ex.printStackTrace();
            
        } finally {
            if (conn != null) {
                // closes the database connection
                try {
                    conn.close();
                } catch (SQLException ex) {
                    //silent
                }
            }
            // sets the message in request scope
            request.setAttribute("message", message);

            // forwards to the message page
            getServletContext().getRequestDispatcher("/Message.jsp")
                .include(request, response);
        }
    }
    
    
    
}